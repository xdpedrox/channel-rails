require 'test_helper'

class DefaultChannelGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @default_channel_group = default_channel_groups(:one)
  end

  test "should get index" do
    get default_channel_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_default_channel_group_url
    assert_response :success
  end

  test "should create default_channel_group" do
    assert_difference('DefaultChannelGroup.count') do
      post default_channel_groups_url, params: { default_channel_group: { level: @default_channel_group.level, name: @default_channel_group.name } }
    end

    assert_redirected_to default_channel_group_url(DefaultChannelGroup.last)
  end

  test "should show default_channel_group" do
    get default_channel_group_url(@default_channel_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_default_channel_group_url(@default_channel_group)
    assert_response :success
  end

  test "should update default_channel_group" do
    patch default_channel_group_url(@default_channel_group), params: { default_channel_group: { level: @default_channel_group.level, name: @default_channel_group.name } }
    assert_redirected_to default_channel_group_url(@default_channel_group)
  end

  test "should destroy default_channel_group" do
    assert_difference('DefaultChannelGroup.count', -1) do
      delete default_channel_group_url(@default_channel_group)
    end

    assert_redirected_to default_channel_groups_url
  end
end
