require 'test_helper'

class AdminRanksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_rank = admin_ranks(:one)
  end

  test "should get index" do
    get admin_ranks_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_rank_url
    assert_response :success
  end

  test "should create admin_rank" do
    assert_difference('AdminRank.count') do
      post admin_ranks_url, params: { admin_rank: { admin_level: @admin_rank.admin_level, name: @admin_rank.name } }
    end

    assert_redirected_to admin_rank_url(AdminRank.last)
  end

  test "should show admin_rank" do
    get admin_rank_url(@admin_rank)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_rank_url(@admin_rank)
    assert_response :success
  end

  test "should update admin_rank" do
    patch admin_rank_url(@admin_rank), params: { admin_rank: { admin_level: @admin_rank.admin_level, name: @admin_rank.name } }
    assert_redirected_to admin_rank_url(@admin_rank)
  end

  test "should destroy admin_rank" do
    assert_difference('AdminRank.count', -1) do
      delete admin_rank_url(@admin_rank)
    end

    assert_redirected_to admin_ranks_url
  end
end
