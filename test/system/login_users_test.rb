require "application_system_test_case"

class LoginUsersTest < ApplicationSystemTestCase
  setup do
    @login_user = login_users(:one)
  end

  test "visiting the index" do
    visit login_users_url
    assert_selector "h1", text: "Login Users"
  end

  test "creating a Login user" do
    visit login_users_url
    click_on "New Login User"

    fill_in "Platform", with: @login_user.platform
    fill_in "Platform", with: @login_user.platform_id
    fill_in "User", with: @login_user.user_id
    click_on "Create Login user"

    assert_text "Login user was successfully created"
    click_on "Back"
  end

  test "updating a Login user" do
    visit login_users_url
    click_on "Edit", match: :first

    fill_in "Platform", with: @login_user.platform
    fill_in "Platform", with: @login_user.platform_id
    fill_in "User", with: @login_user.user_id
    click_on "Update Login user"

    assert_text "Login user was successfully updated"
    click_on "Back"
  end

  test "destroying a Login user" do
    visit login_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Login user was successfully destroyed"
  end
end
