require "application_system_test_case"

class DefaultChannelGroupsTest < ApplicationSystemTestCase
  setup do
    @default_channel_group = default_channel_groups(:one)
  end

  test "visiting the index" do
    visit default_channel_groups_url
    assert_selector "h1", text: "Default Channel Groups"
  end

  test "creating a Default channel group" do
    visit default_channel_groups_url
    click_on "New Default Channel Group"

    fill_in "Level", with: @default_channel_group.level
    fill_in "Name", with: @default_channel_group.name
    click_on "Create Default channel group"

    assert_text "Default channel group was successfully created"
    click_on "Back"
  end

  test "updating a Default channel group" do
    visit default_channel_groups_url
    click_on "Edit", match: :first

    fill_in "Level", with: @default_channel_group.level
    fill_in "Name", with: @default_channel_group.name
    click_on "Update Default channel group"

    assert_text "Default channel group was successfully updated"
    click_on "Back"
  end

  test "destroying a Default channel group" do
    visit default_channel_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Default channel group was successfully destroyed"
  end
end
