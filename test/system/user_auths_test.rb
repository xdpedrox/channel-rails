require "application_system_test_case"

class UserAuthsTest < ApplicationSystemTestCase
  setup do
    @user_auth = user_auths(:one)
  end

  test "visiting the index" do
    visit user_auths_url
    assert_selector "h1", text: "User Auths"
  end

  test "creating a User auth" do
    visit user_auths_url
    click_on "New User Auth"

    click_on "Create User auth"

    assert_text "User auth was successfully created"
    click_on "Back"
  end

  test "updating a User auth" do
    visit user_auths_url
    click_on "Edit", match: :first

    click_on "Update User auth"

    assert_text "User auth was successfully updated"
    click_on "Back"
  end

  test "destroying a User auth" do
    visit user_auths_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User auth was successfully destroyed"
  end
end
