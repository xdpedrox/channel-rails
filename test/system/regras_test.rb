require "application_system_test_case"

class RegrasTest < ApplicationSystemTestCase
  setup do
    @regra = regras(:one)
  end

  test "visiting the index" do
    visit regras_url
    assert_selector "h1", text: "Regras"
  end

  test "creating a Regra" do
    visit regras_url
    click_on "New Regra"

    fill_in "Description", with: @regra.description
    fill_in "Name", with: @regra.name
    click_on "Create Regra"

    assert_text "Regra was successfully created"
    click_on "Back"
  end

  test "updating a Regra" do
    visit regras_url
    click_on "Edit", match: :first

    fill_in "Description", with: @regra.description
    fill_in "Name", with: @regra.name
    click_on "Update Regra"

    assert_text "Regra was successfully updated"
    click_on "Back"
  end

  test "destroying a Regra" do
    visit regras_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Regra was successfully destroyed"
  end
end
