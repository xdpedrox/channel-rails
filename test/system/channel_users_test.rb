require "application_system_test_case"

class ChannelUsersTest < ApplicationSystemTestCase
  setup do
    @channel_user = channel_users(:one)
  end

  test "visiting the index" do
    visit channel_users_url
    assert_selector "h1", text: "Channel Users"
  end

  test "creating a Channel user" do
    visit channel_users_url
    click_on "New Channel User"

    fill_in "Channel", with: @channel_user.channel_id
    fill_in "User", with: @channel_user.user_id
    click_on "Create Channel user"

    assert_text "Channel user was successfully created"
    click_on "Back"
  end

  test "updating a Channel user" do
    visit channel_users_url
    click_on "Edit", match: :first

    fill_in "Channel", with: @channel_user.channel_id
    fill_in "User", with: @channel_user.user_id
    click_on "Update Channel user"

    assert_text "Channel user was successfully updated"
    click_on "Back"
  end

  test "destroying a Channel user" do
    visit channel_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Channel user was successfully destroyed"
  end
end
