require "application_system_test_case"

class AdminRanksTest < ApplicationSystemTestCase
  setup do
    @admin_rank = admin_ranks(:one)
  end

  test "visiting the index" do
    visit admin_ranks_url
    assert_selector "h1", text: "Admin Ranks"
  end

  test "creating a Admin rank" do
    visit admin_ranks_url
    click_on "New Admin Rank"

    fill_in "Admin level", with: @admin_rank.admin_level
    fill_in "Name", with: @admin_rank.name
    click_on "Create Admin rank"

    assert_text "Admin rank was successfully created"
    click_on "Back"
  end

  test "updating a Admin rank" do
    visit admin_ranks_url
    click_on "Edit", match: :first

    fill_in "Admin level", with: @admin_rank.admin_level
    fill_in "Name", with: @admin_rank.name
    click_on "Update Admin rank"

    assert_text "Admin rank was successfully updated"
    click_on "Back"
  end

  test "destroying a Admin rank" do
    visit admin_ranks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Admin rank was successfully destroyed"
  end
end
