Rails.application.routes.draw do

  root "pages#index"

  namespace "admin" do
    get "/" => "dashboard#index"
    resources :regras
    resources :users
    resources :platforms
    resources :user_auths
    resources :channel_users
    resources :channels
    resources :default_channel_groups
    resources :admin_ranks
    resources :zones
  end

  namespace "dashboard" do
    get "/" => "dashboard#index"
    get "/profile" => "dashboard#profile"
    get "/settings" => "dashboard#settings"
    resources :channels
    resources :users
  end

  resources :channels, only: [:index, :new, :create]
  resources :feedback, only: [:index]
  resources :servidores, only: [:index]
  resources :users, only: [:index, :new, :create]

  get "staff" => "pages#staff"
  get "regras" => "pages#regras"
  get "rank" => "pages#rank"
  get "bans" => "pages#bans"

  get 'logout' => "auth#destroy"
  get 'login' => "auth#new"

  get 'auth/failure'
  post 'auth/steam/callback' => 'auth#steam'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
