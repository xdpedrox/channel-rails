# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Platform.new(name: 'steam').save

roles = {}

%i[admin user].each do |name|
  roles[name] = Role.create(name: name)
end

admins = {}

# %w[pedro@sardjv.co.uk].each do |email|
#   user = admins[email] = User.create(email: email)
#   user.roles << roles[:admin]
# end


users = {}

# %w[tinipedro@gmail.com].each do |email|
#   user = users[email] = User.create(email: email)
#   user.roles << roles[:user]
# end

# 15.times do |i|
#   email = "test#{i}@test.com"
#   user = employees[email] = User.create(first_name: 'Doctor', last_name: i, email: email)
#   user.roles << roles[:employee]
# end


zones = [
  [ "Default", 1, 1000, 1],
]

zones.each do |name, next_channel_number, next_spacer_number, last_channel_id|
  Zone.create(name: name, next_channel_number: next_channel_number, next_spacer_number: next_spacer_number, last_channel_id: last_channel_id)
end