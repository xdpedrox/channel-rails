class CreateAuths < ActiveRecord::Migration[6.0]
  def change
    create_table :auths do |t|
      t.string :platform_user_id
      t.belongs_to :platform
      t.timestamps
    end
    add_index(:auths, :platform_user_id, unique: true)
  end
end
