class CreateUserAuths < ActiveRecord::Migration[6.0]
  def change
    create_table :user_auths do |t|
      t.belongs_to :user
      t.belongs_to :auth
      t.belongs_to :platform
      t.timestamps
    end
    
    add_index(:user_auths, [:user_id, :auth_id, :platform_id], unique: true)
  end
end
