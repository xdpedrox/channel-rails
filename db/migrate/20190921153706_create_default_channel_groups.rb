class CreateDefaultChannelGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :default_channel_groups do |t|
      t.string :name
      t.integer :level

      t.timestamps
    end
  end
end
