class CreateChannels < ActiveRecord::Migration[6.0]
  def change
    create_table :channels do |t|
      t.string :status
      t.string :name
      t.belongs_to :zone
      t.integer :channel_order
      t.integer :spacer_number
      t.integer :empty_spacer_cid
      t.integer :spacer_bar_cid
      t.integer :server_group_id
      t.datetime :last_used

      t.timestamps
    end
    
    add_index(:channels, :name, unique: true)
    add_index(:channels, :empty_spacer_cid, unique: true)
    add_index(:channels, :spacer_bar_cid, unique: true)
    add_index(:channels, :server_group_id, unique: true)
    add_index(:channels, [:channel_order, :zone_id], unique: true)

  end
end
