class CreateZones < ActiveRecord::Migration[6.0]
  def change
    create_table :zones do |t|
      t.string :name
      t.integer :next_channel_number
      t.integer :next_spacer_number
      t.integer :last_channel_id

      t.timestamps
    end
  end
end
