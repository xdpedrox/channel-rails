class CreateChannelUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :channel_users do |t|
      t.belongs_to :channel
      t.belongs_to :user
      t.timestamps
    end
  end
end
