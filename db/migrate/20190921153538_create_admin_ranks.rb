class CreateAdminRanks < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_ranks do |t|
      t.string :name
      t.integer :admin_level

      t.timestamps
    end
  end
end
