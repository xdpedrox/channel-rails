class CreateAdmins < ActiveRecord::Migration[6.0]
  def change
    create_table :admins do |t|
      t.string :name
      t.belongs_to :user
      t.belongs_to :admin_rank
      t.timestamps
    end
  end
end
