# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  status     :string(255)
#  nickname   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord

    has_many :channel_user
    has_many :channel, through: :channel_user

    has_many :role_users
    has_many :roles, through: :role_users

    has_many :user_auth, :dependent => :destroy
    has_many :auth, through: :user_auth
    has_many :platform, through: :user_auth


    validates :name, presence: true
    validates :status, inclusion: { in: %w(good unverified banned),
        message: "%{value} is not a valid status" }
    

    
  scope :with_role, (lambda do |name|
    joins(:roles)
    .where(roles: { name: name })
    .distinct
  end)

  class << self
    def authenticate(args)
      where(args).first
    end
  end

  def logged_in?
    true
  end

  def role?(name)
    roles.where(name: name).any?
  end

  def admin?
    role?(:admin)
  end

  def user?
    role?(:user)
  end

  
  # def platforms?(name)
  #   platform.where(name: name).any?

  # end

  # def platform?
  #   platforms?(:steam)
  # end

end

