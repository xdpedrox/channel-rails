module NullObjects

    class User
      def name
        "Null User"
      end
  
      def image_url
        nil
      end
  
      def logged_in?
        false
      end
    end
    
  end
  
  