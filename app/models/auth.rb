# == Schema Information
#
# Table name: auths
#
#  id               :bigint           not null, primary key
#  platform_user_id :string(255)
#  platform_id      :bigint
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Auth < ApplicationRecord
    has_one :user_auth, :dependent => :destroy
    has_one :platform, through: :user_auth
    has_one :auth, through: :user_auth
    has_one :user, through: :user_auth

    def logged_in?
        true
    end

end
