# == Schema Information
#
# Table name: zones
#
#  id                  :bigint           not null, primary key
#  name                :string(255)
#  next_channel_number :integer
#  next_spacer_number  :integer
#  last_channel_id     :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Zone < ApplicationRecord
    has_many :channel
end
