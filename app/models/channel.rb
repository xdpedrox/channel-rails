# == Schema Information
#
# Table name: channels
#
#  id               :bigint           not null, primary key
#  status           :string(255)
#  name             :string(255)
#  zone_id          :bigint
#  channel_order    :integer
#  spacer_number    :integer
#  empty_spacer_cid :integer
#  spacer_bar_cid   :integer
#  server_group_id  :integer
#  last_used        :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Channel < ApplicationRecord
    
    belongs_to :zone
    has_many :channel_user
    has_many :users, through: :channel_user

    validates :name, presence: true
    validates :status, inclusion: { in: %w(good, unused, deleted),
        message: "%{value} is not a valid status" }
    
end
