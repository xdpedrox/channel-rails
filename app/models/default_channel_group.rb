# == Schema Information
#
# Table name: default_channel_groups
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  level      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DefaultChannelGroup < ApplicationRecord
end
