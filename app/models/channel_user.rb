# == Schema Information
#
# Table name: channel_users
#
#  id         :bigint           not null, primary key
#  channel_id :bigint
#  user_id    :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChannelUser < ApplicationRecord
    belongs_to :user
    belongs_to :channel
end
