# == Schema Information
#
# Table name: user_auths
#
#  id          :bigint           not null, primary key
#  user_id     :bigint
#  auth_id     :bigint
#  platform_id :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class UserAuth < ApplicationRecord
    belongs_to :user
    belongs_to :auth
    belongs_to :platform 

    validates :auth_id, uniqueness: true
end
