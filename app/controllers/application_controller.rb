class ApplicationController < ActionController::Base
    extend ActiveSupport::Concern

    layout "application"


    helper_method :current_user, :logged_in?

    
    included do
        before_filder :set_page_defaults
    end

    def logged_in?
        !session['current_user'].nil?
      end

      
    def set_page_defaults
        @page_title = 'Channel Website'
    end


end
