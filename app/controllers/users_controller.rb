  class UsersController < ApplicationController
    before_action :user_params, only: [:show, :edit, :update, :destroy]


    def show
    end

    # GET /users/new
    def new
      @member = User.new
    end

    # POST /users
    # POST /users.json
    def create
      
      binding.pry
      
      user = user_params && UserService.fetch_user_from_auth(user_params, session[:auth_user_id])

      if auth_user.logged_in?

        session[:user_id] = user.id

        redirect_to "/login"
      else
        redirect_to "/users/new"
      end
    end

    def user_params
      params.require(:user).permit(:name, :nickname)
    end
  end
