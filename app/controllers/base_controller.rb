class BaseController < ApplicationController
    layout 'application'
  
    include AuthenticatedControllerConcerns
  
    # rescue_from ActionController::UnknownFormat, with: :unknown_format
  
    # private
  
    # def unknown_format(exception)
    #   render plain: 'Unsupported Format', status: 406
    # end
  end
  