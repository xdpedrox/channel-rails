module Admin
  class DefaultChannelGroupsController < Admin::BaseAdminController
    before_action :set_default_channel_group, only: [:show, :edit, :update, :destroy]

    # GET /default_channel_groups
    # GET /default_channel_groups.json
    def index
      @default_channel_groups = DefaultChannelGroup.all
    end

    # GET /default_channel_groups/1
    # GET /default_channel_groups/1.json
    def show
    end

    # GET /default_channel_groups/new
    def new
      @default_channel_group = DefaultChannelGroup.new
    end

    # GET /default_channel_groups/1/edit
    def edit
    end

    # POST /default_channel_groups
    # POST /default_channel_groups.json
    def create
      @default_channel_group = DefaultChannelGroup.new(default_channel_group_params)

      respond_to do |format|
        if @default_channel_group.save
          format.html { redirect_to @default_channel_group, notice: 'Default channel group was successfully created.' }
          format.json { render :show, status: :created, location: @default_channel_group }
        else
          format.html { render :new }
          format.json { render json: @default_channel_group.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /default_channel_groups/1
    # PATCH/PUT /default_channel_groups/1.json
    def update
      respond_to do |format|
        if @default_channel_group.update(default_channel_group_params)
          format.html { redirect_to @default_channel_group, notice: 'Default channel group was successfully updated.' }
          format.json { render :show, status: :ok, location: @default_channel_group }
        else
          format.html { render :edit }
          format.json { render json: @default_channel_group.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /default_channel_groups/1
    # DELETE /default_channel_groups/1.json
    def destroy
      @default_channel_group.destroy
      respond_to do |format|
        format.html { redirect_to default_channel_groups_url, notice: 'Default channel group was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_default_channel_group
        @default_channel_group = DefaultChannelGroup.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def default_channel_group_params
        params.require(:default_channel_group).permit(:name, :level)
      end
  end
end