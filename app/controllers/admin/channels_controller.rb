module Admin
  class ChannelsController < Admin::BaseAdminController
    before_action :set_channel, only: [:show, :edit, :update, :destroy]
    
    require 'rest-client'
    require 'json'

    # GET /channels
    # GET /channels.json
    def index
      @channels = Channel.all
    end

    # GET /channels/1
    # GET /channels/1.json
    def show
    end

    # GET /channels/new
    def new
      @channel = Channel.new
    end

    # GET /channels/1/edit
    def edit
    end

    # POST /channels
    # POST /channels.json
    def create
      # @channel = Channel.new(channel_params)


      @bodyd = {
        clid: 1,
        move: false,
        areaName: 'pedro',
        areaNextSpacerNumber: 1,
        areaNextChannelNumber: 1,
        areaLastChannelId: 1,
        channelName: 'pedro',
        channelPassword: '123',
        channelTopic: 'topic',
        channelDescription: 'description'
      }
      
      # json = JSON.encode(@bodyd)
      url = 'http://localhost:1337/channel/create'
      
      # json = ActiveSupport::JSON.encode(@bodyd)
      # puts json
      # url = 'https://api.spotify.com/v1/search?type=artist&q=tycho'
      response  = RestClient.post url, @bodyd
      
      

      # 
      # puts JSON.parse(response)
      
    
      


      # permitted = params.require(:person).permit(:name, :age)


      # response = HTTParty.post(url, body: @bodyd))\
      
      # HTTParty.get(url)
      
      #  response.parsed_response
      
      # binding.pry
      

      respond_to do |format|
        if @channel.save
          format.html { redirect_to @channel, notice: 'Channel was successfully created.' }
          format.json { render :show, status: :created, location: @channel }
        else
          format.html { render :new }
          format.json { render json: @channel.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /channels/1
    # PATCH/PUT /channels/1.json
    def update
      respond_to do |format|
        if @channel.update(channel_params)
          format.html { redirect_to @channel, notice: 'Channel was successfully updated.' }
          format.json { render :show, status: :ok, location: @channel }
        else
          format.html { render :edit }
          format.json { render json: @channel.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /channels/1
    # DELETE /channels/1.json
    def destroy
      @channel.destroy
      respond_to do |format|
        format.html { redirect_to channels_url, notice: 'Channel was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_channel
        @channel = Channel.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def channel_params
        params.require(:channel).permit(:status, :name, :channel_order, :spacer_number, :empty_spacer_cid, :spacer_bar_cid, :group_id, :last_used, :zone_id)
      end
  end
end