module Admin
  class UsersController < Admin::BaseAdminController

    before_action :set_user, only: [:show, :edit, :update, :destroy]

    # GET /users
    # GET /users.json
    def index
      @users = User.all
      puts request.env['omniauth.auth']
    end


    # GET /users/1
    # GET /users/1.json
    def show
    end

    # GET /users/new
    def new
      @user = User.new
    end

    # GET /users/1/edit
    def edit
    end

    # POST /users
    # POST /users.json
    def create
      # user_params
    

      user_params = user_params

      params['status'] = 'good'
      
      # @user = User.new(@my_params)

      # respond_to do |format|
      #   if @user.save
          
        @user_auth = UserAuth.new(user_params)
        
        if auth_user.logged_in? 

          session[:current_user] = { 
            :nickname => @user['nickname'],
            :user_id => @user['id'],
          }

          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else 
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

    end

    # PATCH/PUT /users/1
    # PATCH/PUT /users/1.json
    def update
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /users/1
    # DELETE /users/1.json
    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        params.require(:user).permit(:name, :status, :nickname)
      end
  end
end