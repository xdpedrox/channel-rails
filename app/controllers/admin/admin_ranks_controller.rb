module Admin
  class AdminRanksController < Admin::BaseAdminController

    before_action :set_admin_rank, only: [:show, :edit, :update, :destroy]

    # GET /admin_ranks
    # GET /admin_ranks.json
    def index
      @admin_ranks = Role.all
    end

    # GET /admin_ranks/1
    # GET /admin_ranks/1.json
    def show
    end

    # GET /admin_ranks/new
    def new
      @admin_rank = Role.new
    end

    # GET /admin_ranks/1/edit
    def edit
    end

    # POST /admin_ranks
    # POST /admin_ranks.json
    def create
      @admin_rank = Role.new(admin_rank_params)

      respond_to do |format|
        if @admin_rank.save
          format.html { redirect_to @admin_rank, notice: 'Admin rank was successfully created.' }
          format.json { render :show, status: :created, location: @admin_rank }
        else
          format.html { render :new }
          format.json { render json: @admin_rank.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin_ranks/1
    # PATCH/PUT /admin_ranks/1.json
    def update
      respond_to do |format|
        if @admin_rank.update(admin_rank_params)
          format.html { redirect_to @admin_rank, notice: 'Admin rank was successfully updated.' }
          format.json { render :show, status: :ok, location: @admin_rank }
        else
          format.html { render :edit }
          format.json { render json: @admin_rank.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin_ranks/1
    # DELETE /admin_ranks/1.json
    def destroy
      @admin_rank.destroy
      respond_to do |format|
        format.html { redirect_to admin_ranks_url, notice: 'Admin rank was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_admin_rank
        @admin_rank = Role.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def admin_rank_params
        params.require(:admin_rank).permit(:name, :admin_level)
      end
  end
end