module Admin
  class BaseAdminController < BaseController
    before_filter :set_page_title

    def set_page_title
      @page_title = "Default Title"
    end
  end
end