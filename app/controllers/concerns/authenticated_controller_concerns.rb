module AuthenticatedControllerConcerns
    extend ActiveSupport::Concern
  
    included do
      before_action :require_logged_in_user
      helper_method :current_user, :logged_in?
    end
  
    private
  
    module ClassMethods
      def require_role(*names)
        names.map {|name| roles_required << name.to_sym }
        before_action :require_role
      end
  
      def roles_required
        @roles_required ||= Set.new
      end
    end
  
    def require_logged_in_user
      redirect_to login_path unless logged_in?
    end
  
    def require_role
      unless self.class.roles_required.any? { |name| current_user.role?(name) }
        redirect_to dashboard_path, alert: 'Insufficient access'
      end
    end
  
    def current_user
      @current_user ||= User.where(id: session[:user_id]).first
    end
  
    def logged_in?
      !current_user.nil?
    end
  
    # def current_ability
    #   @current_ability ||= UserAbility.new(current_user)
    # end
  end
  