class AuthController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:steam]

  def new
    # login page and auth0/failure
    # redirect_to '/users' if logged_in?
  end

  def destroy
    reset_session
    flash[:notice] = "You have been logged out."
    redirect_to root_path
  end

  def failure
    flash[:error] = request.params['message']
    redirect_to root_path
  end  

  def steam
    steam_response = request.env['omniauth.auth']
    auth_user = steam_response && AuthService.fetch_user_auth_from_steam(steam_response)
  
    if auth_user.logged_in? 

        session[:auth_user_id] = auth_user.id

        flash[:notice] = 'You have successfully logged in.'
        redirect_to new_user_path
    else
      redirect_to root_path
    end
  end

end
