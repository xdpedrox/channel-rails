
// libraries
import React from 'react';

import CreateChannel from './CreateChannel';

const app = props => (<CreateChannel {...props}/>);

export default app;

// eof
