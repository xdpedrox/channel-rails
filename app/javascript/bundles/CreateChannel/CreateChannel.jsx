import React, { useState } from 'react';
import axios from '../axios';

const CreateChannel = (props) => {
  console.log(props)
  const [channelName, setChannelName] = useState("");

  const onNameChange = (input) => {
    setChannelName(input.target.value)
  }

  const submit = () => {
    console.log(props.createUrl)
    axios.post(props.createUrl, {
      name: channelName,
      password: 'lol'
    })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      // debugger
      console.log(error);
    });
  }

  return (
    <div className="row">
      <div className="col-md-6">
        <div className="kt-portlet">
          <div className="kt-portlet__head">
            <div className="kt-portlet__head-label">
              <h3 className="kt-portlet__head-title">
                Base Controls
             </h3>
            </div>
          </div>
            <div className="kt-portlet__body">
              <div className="form-group">
                <label>Nomde do Canal</label>
                <input value={channelName} onChange={onNameChange} className="form-control" placeholder="Enter email" type="text"></input>
                <span className="form-text text-muted">We'll never share your email with anyone else.</span>
              </div>
            </div>
            <div className="kt-portlet__foot">
              <div className="kt-form__actions">
                <button className="btn btn-primary" onClick={submit} type="submit">Submit</button>
                <button className="btn btn-secondary" type="reset">Cancel</button>
                <span></span>
              </div>
            </div>
          </div>
      </div>
    </div>

  )

}

export default CreateChannel;
