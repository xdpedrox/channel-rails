json.extract! admin_rank, :id, :name, :admin_level, :created_at, :updated_at
json.url admin_rank_url(admin_rank, format: :json)
