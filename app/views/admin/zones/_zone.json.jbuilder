json.extract! zone, :id, :name, :next_channel_number, :next_spacer_number, :last_channel_id, :created_at, :updated_at
json.url admin_zone_url(zone, format: :json)
