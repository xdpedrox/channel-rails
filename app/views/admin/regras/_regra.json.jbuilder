json.extract! regra, :id, :name, :description, :created_at, :updated_at
json.url regra_url(regra, format: :json)
