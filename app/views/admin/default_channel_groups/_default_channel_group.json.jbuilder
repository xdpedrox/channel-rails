json.extract! default_channel_group, :id, :name, :level, :created_at, :updated_at
json.url default_channel_group_url(default_channel_group, format: :json)
