json.extract! channel, :id, :status, :name, :channel_order, :spacer_number, :empty_spacer_cid, :spacer_bar_cid, :group_id, :last_used, :zone_id, :created_at, :updated_at
json.url channel_url(channel, format: :json)
