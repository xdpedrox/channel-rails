module Teamspeak

  class CreateTeam
    require 'rest-client'
    require 'pry'

    attr_reader :params

    def initialize(params)
      @params = params
    end

    def self.call
      
      @default_zone = 1

      @zone = Zone.find(1)

      channel_name = "D" + @zone.next_channel_number + @params[:name]

      binding.pry

      response = RestClient.post 'http://127.0.0.1:1337/team', {
        name: channel_name,
        password: @params[:password],
        topic: "topic",
        description: "description",
        nextSpacerNumber: @zone.nextSpacerNumber,
        lastChannelId: @zone.last_channel_id,
        uuid: "smgDpLgFrZmV2FrK/PwWvFCA2Pw=",
        channelGroupId: 5,
        move: @params[:move]
      }

      parsed_response = JSON.parse(response.body)
      
      if (parsed_response["code"] == 200) 
        parsed_response["data"]

        Channel.all
        
        puts 'Success'
      end

      binding.pry
      
    end

    def parsed_response
      @parsed_response || JSON.parse(response.body)
    end
    
  end

  # class APIResponder < ActionController::Responder

  #   private

  #   def display(resource, options = {})
  #     super(resource.data, options)
  #   end

  #   def has_errors?
  #     !resource.success?
  #   end

  #   def json_resource_errors
  #     { error: resource.error, message: resource.error_message, code: resource.code, details: resource.details }
  #   end

  #   def api_location
  #     nil
  #   end
  # end

  class Success
    attr_reader :data
    def initialize(data)
      @data = data
    end

    def success?
      true
    end
  end

  class Error
    attr_reader :error, :code, :details
    def initialize(error = nil, code = nil, details = nil)
      @error = error
      @code = code
      @details = details
    end

    def error_message
      error.to_s
    end

    def success?
      false
    end
  end

  class ValidationError < Error
    def initialize(details)
      super(:validation_failed, 101, details)
    end

    def error_message
      "Validation failed: see details"
    end
  end

  # class InvoicesController < ApplicationController
  #   respond_to :json

  #   def create
  #     form = InvoiceForm.new(params)
  #     result = CreateInvoice.new(current_user, form).call # => ValidationError.new(invoice.errors)
  #     respond_with result # { error: "validation_error", code: 101, message: "..." details: { ... } }
  #   end


  #   def update
  #     result = UpdateInvoice.new(current_user, params[:id]).call # => Success.new(invoice)
  #     respond_with(result) # { billing_date: ..., company_name: ... }
  #   end

  #   # ...

  #   def self.responder
  #     APIResponder
  #   end
  # end

end
