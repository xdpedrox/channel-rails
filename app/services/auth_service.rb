class AuthService
  def self.fetch_user_auth_from_steam(data)
    return NullObjects::Auth.new if data.nil?

    Auth.find_or_create_by(platform_id: 1, platform_user_id: data.uid)
  end
end
  