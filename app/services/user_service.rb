class UserService
  def self.fetch_user_from_auth(data, auth_id)
    return NullObjects::User.new if data.nil?

    # Find AuthUser
    user_auth = UserAuth.includes(:user).where('user_auths.auth_id' == data['id'])

    if !user_auth.empty?
      puts 'Found'
      User.find(user_auth[0].user_id)
    else
      puts 'Create'

      # binding.prys
      user_record = User.new do |r|
        r.name = data['name']
        r.nickname = data['nickname']
        r.status = 'good'
      end

      UserAuth.new(user_id: user_record['id'], auth_id: auth_id, platform_id: data['platform'])

      return user_record

    end

  end
end
